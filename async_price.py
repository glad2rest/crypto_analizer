# !/usr/bin/python

# ↓ scrabbing
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from webdriver_manager.chrome import ChromeDriverManager

# ↓ async
import datetime
import asyncio

# ↓ debug
import json


SESSION = dict(rate=dict())
DYNAMICS = dict()

class Watcher():
    def __init__(self, web_driver):
        self.web_driver = web_driver
        self.last_price = None

    async def scrab_data_ethusdt(self, link):
        """
        receives: link 
            Establishes connection using the selenium-library (any)
        returns: ethusdt_price (str)
            Actual ETH-USDT price.
        """
        global SESSION

        self.web_driver.get(link)
        # waits until site's data gathered and defines 'ethusdt_price' variable with str() containing actual price
        ethusdt_price = [price.text for price in WebDriverWait(self.web_driver, 20).until(EC.visibility_of_all_elements_located((By.XPATH, "//span[@class='last-jXw2qXFy js-symbol-last'][1]")))][0]

        date_time = datetime.datetime.now()
        time = date_time.strftime('%H:%M:%S')

        # due to site details 'ethusdt' variable can be empty
        # we'll check if 'ethusdth' has value and change 'last_price' variable once in the method
        if self.last_price is None:
            self.last_price = ethusdt_price

        # defines 'updates' list of dicts to fill it with actual data: 'rate' with values of the variable 'ethusdt_price' and time
        updates = dict()
        if ethusdt_price:
            updates = {
                time: {ethusdt_price: f'{float(self.last_price) - float(ethusdt_price):.2f} %'}
                
            }

            # updates 'SESSION' dict with the new values getting from 'updates'-list once
            if not SESSION['rate']:
                SESSION['rate'].update(updates)

            # periodically updates 'SESSION' if last price in the dict changes    
            if self.last_price != ethusdt_price and ethusdt_price != list(price for element in SESSION['rate'].values() for price in element)[-1]:
                SESSION['rate'].update(updates)

            # print(json.dumps(SESSION, sort_keys=True, indent=4))
        return ethusdt_price
 
    async def main(self, link):
        '''
        receives: link
        returns: None
            prints the dynamic of the ETH-USDT rate every one hour if dynamic greater than 1 percent
        '''
        while True:
            # waiting for data from the 'scrab_data' method
            await self.scrab_data_ethusdt(link=link)

            old_price = float(self.last_price)
            # defines the datetime-object
            time_start = datetime.datetime.strptime(list(SESSION['rate'].keys())[0], '%H:%M:%S') 

            # parsing through the 'SESSION' dict
            for time_string, rate_data in SESSION['rate'].items():
                new_price = float(list(rate_data.keys())[0]) # getting every last price from the 'SESSION' dict
                diff_prices = abs(old_price - new_price) # counts the dynamics 

                time_data = datetime.datetime.strptime(time_string, '%H:%M:%S')

                # if dynamics will be greater than 1 percent prints message and changes 'last_price' variable to start counting again
                if diff_prices >= old_price * 0.01 and time_data - time_start >= datetime.timedelta(minutes=5):
                    print(f'At {time_data.time()} - ETHUSDT rate has been changed by {diff_prices / old_price * 100:.2f}%:\nWas: {old_price} - Became: {new_price}')
                    self.last_price = new_price


if __name__ == '__main__':
    watcher = Watcher(webdriver.Chrome(service=Service(ChromeDriverManager().install())))
    asyncio.run(watcher.main('https://www.tradingview.com/symbols/ETHUSDT/'))